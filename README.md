---
pipeline_tag: text-to-image
---
LEOSAM's MoonFilm, but fp16/cleaned - smaller size, same result.
======== 
///
**[**original checkpoint link**](https://civitai.com/models/43977/leosams-moonfilm)**
*(all rights to the model belong to LEOSAM)*

---
*[*grid 01*](https://huggingface.co/datasets/fp16-guy/grids/blob/main/leosams%20moonfilm%20film%20grain%2020%2001%2020230807124520-111-leosamsMoonfilm_filmGrain20-Euler%20a-6.png)  *(1.99gb version)*

*[*grid 02*](https://huggingface.co/datasets/fp16-guy/grids/blob/main/leosams%20moonfilm%20film%20grain%2020%2002%2020230807124615-111-leosamsMoonfilm_filmGrain20-Euler%20a-6.png) *(1.83gb version - no vae)*

*[*grid 03*](https://huggingface.co/datasets/fp16-guy/grids_inp/blob/main/leosamsMoonfilm_filmGrain20%20inp%2001%2020230815215859-111-leosamsMoonfilm_filmGrain20_fp16-Euler%20a-5.5.png)  *(1.99gb inpainting version)*

*[*grid 04*](https://huggingface.co/datasets/fp16-guy/grids_inp/blob/main/leosamsMoonfilm_filmGrain20%20inp%2002%2020230815220006-111-leosamsMoonfilm_filmGrain20_fp16_no_vae-Euler%20a-5.5.png) *(1.83gb inpainting version - no vae)*
